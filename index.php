<?php
require('app/core/autoloader.php');

//define routes
Router::get('', 'welcome@index');
Router::get('sample', 'welcome@sample');

Router::get('user', 'user@user');

Router::get('user/add', 'user@add');
Router::post('user/add', 'user@add');

Router::get('user/edit/(:num)', 'user@edit');
Router::post('user/edit/(:num)', 'user@edit');

Router::get('user/del/(:num)', 'user@del');
Router::post('user/del/(:num)', 'user@del');

//Ingrediens
Router::get('ingredient', 'ingredient@ingredient');
Router::get('ingredient/add', 'ingredient@add');
Router::post('ingredient/add', 'ingredient@add');
Router::get('ingredient/edit/(:num)', 'ingredient@edit');
Router::post('ingredient/edit/(:num)', 'ingredient@edit');
Router::get('ingredient/delete/(:num)', 'ingredient@delete');
Router::post('ingredient/delete/(:num)', 'ingredient@delete');

// Orders
Router::get('order', 'order@index');

Router::get('order/details/(:num)', 'order@details');

Router::get('order/place', 'order@place');
Router::post('order/place', 'order@place');

Router::get('order/update/(:num)', 'order@update');
Router::post('order/update/(:num)', 'order@update');

Router::get('order/status/(:num)/(:any)', 'order@change_status');

// Login
Router::get('login', 'login@login');
Router::post('login', 'login@login');

Router::get('logout', 'login@logout');

Router::get('user/password', 'login@password');
Router::post('user/password', 'login@password');

//define routes api
Router::get('api', 'api/login@index');

Router::post('api/login', 'api/login@login');
Router::get('api/logout', 'api/login@logout');

Router::get('api/user', 'api/user@view_all');
Router::get('api/user/(:num)', 'api/user@view');

Router::get('api/order', 'api/order@index');
Router::get('api/order/status', 'api/order@status_list');
Router::get('api/order/status/(:num)/(:any)', 'api/order@change_status');
Router::post('api/order/place', 'api/order@place');

//if no route found
Router::error('error@index');

//execute matched routes
Router::dispatch();
ob_flush();
