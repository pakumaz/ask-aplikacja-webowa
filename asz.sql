﻿-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas wygenerowania: 07 Sie 2014, 13:06
-- Wersja serwera: 5.6.14
-- Wersja PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `asz`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Adresy`
--

CREATE TABLE IF NOT EXISTS `Adresy` (
  `id_adres` int(11) NOT NULL,
  `ulica` varchar(20) DEFAULT NULL,
  `numer` varchar(10) NOT NULL,
  `kod_pocztowy` int(11) NOT NULL,
  `miejscowosc` varchar(20) NOT NULL,
  `Restauracja_id_restauracja` int(11) DEFAULT NULL,
  `Konta_id_konta` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_adres`),
  KEY `fk_Adresy_Restauracja1_idx` (`Restauracja_id_restauracja`),
  KEY `fk_Adresy_Konta1_idx` (`Konta_id_konta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Dania`
--

CREATE TABLE IF NOT EXISTS `Dania` (
  `id_dania` int(11) NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `cena` float NOT NULL,
  PRIMARY KEY (`id_dania`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Dania`
--

INSERT INTO `Dania` (`id_dania`, `nazwa`, `cena`) VALUES
(0, 'Zupa ogórkowa', 3.5),
(1, 'Kotlet mielony', 6),
(2, 'Kopytka', 2.4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Dania-Skladniki`
--

CREATE TABLE IF NOT EXISTS `Dania-Skladniki` (
  `Dania_id_dania` int(11) NOT NULL,
  `Skladniki_id_skladniki` int(11) NOT NULL,
  KEY `fk_Dania_has_Skladniki_Skladniki1_idx` (`Skladniki_id_skladniki`),
  KEY `fk_Dania_has_Skladniki_Dania1_idx` (`Dania_id_dania`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Dania-Zamowienia`
--

CREATE TABLE IF NOT EXISTS `Dania-Zamowienia` (
  `Dania_id_dania` int(11) NOT NULL,
  `Zamowienia_id_zamowienia` int(11) NOT NULL,
  KEY `fk_Dania_has_Zamowienia_Zamowienia1_idx` (`Zamowienia_id_zamowienia`),
  KEY `fk_Dania_has_Zamowienia_Dania1_idx` (`Dania_id_dania`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Dania-Zamowienia`
--

INSERT INTO `Dania-Zamowienia` (`Dania_id_dania`, `Zamowienia_id_zamowienia`) VALUES
(0, 0),
(1, 0),
(1, 1),
(2, 1),
(0, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Konta`
--

CREATE TABLE IF NOT EXISTS `Konta` (
  `id_konta` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(30) NOT NULL,
  `telefon` int(11) DEFAULT NULL,
  `Typy_uzytkownikow_id_typ` int(11) NOT NULL,
  PRIMARY KEY (`id_konta`),
  KEY `fk_Konta_Typy_uzytkownikow_idx` (`Typy_uzytkownikow_id_typ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Konta`
--

INSERT INTO `Konta` (`id_konta`, `login`, `haslo`, `email`, `imie`, `nazwisko`, `telefon`, `Typy_uzytkownikow_id_typ`) VALUES
(0, 'user', '$2y$12$F8OHMHmJ9S.rA/9j3ufco.3CDSqaUekyAONXuCtzzN.TFzViDz9BK', 'user@user.pl', 'Userśźćąśź', 'łóóśąłUser', 123, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Konta-Restauracja`
--

CREATE TABLE IF NOT EXISTS `Konta-Restauracja` (
  `Konta_id_konta` int(11) NOT NULL,
  `Restauracja_id_restauracja` int(11) NOT NULL,
  KEY `fk_Konta_has_Restauracja_Restauracja1_idx` (`Restauracja_id_restauracja`),
  KEY `fk_Konta_has_Restauracja_Konta1_idx` (`Konta_id_konta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Restauracja`
--

CREATE TABLE IF NOT EXISTS `Restauracja` (
  `id_restauracja` int(11) NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `telefon` int(11) DEFAULT NULL,
  `opis` text,
  PRIMARY KEY (`id_restauracja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Restauracja`
--

INSERT INTO `Restauracja` (`id_restauracja`, `nazwa`, `telefon`, `opis`) VALUES
(0, 'Restauracja', '123456789', 'Przykładowa restauracja');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Skladniki`
--

CREATE TABLE IF NOT EXISTS `Skladniki` (
  `id_skladniki` int(11) NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `ilosc` float NOT NULL,
  PRIMARY KEY (`id_skladniki`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Skladniki`
--

INSERT INTO `Skladniki` (`id_skladniki`, `nazwa`, `ilosc`) VALUES
(0, 'Ziemniaki', 12),
(1, 'Mięso mielone', 12),
(2, 'Porcja rosołowa', 12);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Status`
--

CREATE TABLE IF NOT EXISTS `Status` (
  `id_status` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Status`
--

INSERT INTO `Status` (`id_status`, `status`) VALUES
(1, 'realized'),
(2, 'new'),
(3, 'cancel');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Typy_uzytkownikow`
--

CREATE TABLE IF NOT EXISTS `Typy_uzytkownikow` (
  `id_typ` int(11) NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `dodanie_zamowienia` tinyint(1) DEFAULT NULL,
  `usuniecie_zamowienia` tinyint(1) DEFAULT NULL,
  `dodanie_uzytkownika` tinyint(1) DEFAULT NULL,
  `usuniecie_uzytkownika` tinyint(1) DEFAULT NULL,
  `zmiana_ceny` tinyint(1) DEFAULT NULL,
  `dodanie_towaru` tinyint(1) DEFAULT NULL,
  `usuniecie_towaru` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_typ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Typy_Uzytkownikow`
--

INSERT INTO `Typy_uzytkownikow` (`id_typ`, `nazwa`, `dodanie_zamowienia`, `usuniecie_zamowienia`, `dodanie_uzytkownika`, `usuniecie_uzytkownika`, `zmiana_ceny`, `dodanie_towaru`, `usuniecie_towaru`) VALUES
(0, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Zamowienia`
--

CREATE TABLE IF NOT EXISTS `Zamowienia` (
  `id_zamowienia` int(11) NOT NULL AUTO_INCREMENT,
  `ilosc` int(11) NOT NULL,
  `zaplacone` tinyint(1) DEFAULT NULL,
  `Faktura_nr_faktury` int(11) DEFAULT NULL,
  `Restauracja_id_restauracja` int(11) NOT NULL,
  `Konta_id_konta` int(11) NOT NULL,
  `nr_stolika` int(11) DEFAULT NULL,
  `uwagi_klienta` text,
  `notatki` text,
  `data_zlozenia` datetime DEFAULT NULL,
  `data_wydania` datetime DEFAULT NULL,
  `Status_id_status` int(11) NOT NULL,
  PRIMARY KEY (`id_zamowienia`),
  KEY `fk_Zamowienia_Restauracja1_idx` (`Restauracja_id_restauracja`),
  KEY `fk_Zamowienia_Konta1_idx` (`Konta_id_konta`),
  KEY `fk_Zamowienia_Status1_idx` (`Status_id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ograniczenia dla zrzutów tabel
--

INSERT INTO `Zamowienia` (`id_zamowienia`, `ilosc`, `zaplacone`, `Faktura_nr_faktury`, `Restauracja_id_restauracja`, `Konta_id_konta`, `nr_stolika`, `uwagi_klienta`, `notatki`, `data_zlozenia`, `data_wydania`, `Status_id_status`) VALUES
(0, 1, NULL, NULL, 0, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Fusce ut nisl elit. Fusce pulvinar mollis nunc sed dapibus. Donec sapien felis, dictum ut magna at, pharetra tempus lacus.', NULL, NULL, 1),
(1, 1, NULL, NULL, 0, 0, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Fusce ut nisl elit. Fusce pulvinar mollis nunc sed dapibus. Donec sapien felis, dictum ut magna at, pharetra tempus lacus.', NULL, NULL, 2),
(2, 0, NULL, NULL, 0, 0, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Fusce ut nisl elit. Fusce pulvinar mollis nunc sed dapibus. Donec sapien felis, dictum ut magna at, pharetra tempus lacus.', NULL, NULL, 3);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `Adresy`
--
ALTER TABLE `Adresy`
  ADD CONSTRAINT `fk_Adresy_Konta` FOREIGN KEY (`Konta_id_konta`) REFERENCES `Konta` (`id_konta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Adresy_Restauracja` FOREIGN KEY (`Restauracja_id_restauracja`) REFERENCES `Restauracja` (`id_restauracja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `Dania-Skladniki`
--
ALTER TABLE `Dania-Skladniki`
  ADD CONSTRAINT `fk_Dania_has_Skladniki_Dania` FOREIGN KEY (`Dania_id_dania`) REFERENCES `Dania` (`id_dania`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Dania_has_Skladniki_Skladniki` FOREIGN KEY (`Skladniki_id_skladniki`) REFERENCES `Skladniki` (`id_skladniki`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `Dania-Zamowienia`
--
ALTER TABLE `Dania-Zamowienia`
  ADD CONSTRAINT `fk_Dania_has_Zamowienia_Dania` FOREIGN KEY (`Dania_id_dania`) REFERENCES `Dania` (`id_dania`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Dania_has_Zamowienia_Zamowienia1` FOREIGN KEY (`Zamowienia_id_zamowienia`) REFERENCES `Zamowienia` (`id_zamowienia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `Konta`
--
ALTER TABLE `Konta`
  ADD CONSTRAINT `fk_Konta_Typy_uzytkownikow` FOREIGN KEY (`Typy_uzytkownikow_id_typ`) REFERENCES `Typy_uzytkownikow` (`id_typ`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `Konta-Restauracja`
--
ALTER TABLE `Konta-Restauracja`
  ADD CONSTRAINT `fk_Konta_has_Restauracja_Konta` FOREIGN KEY (`Konta_id_konta`) REFERENCES `Konta` (`id_konta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Konta_has_Restauracja_Restauracja` FOREIGN KEY (`Restauracja_id_restauracja`) REFERENCES `Restauracja` (`id_restauracja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `Zamowienia`
--
ALTER TABLE `Zamowienia`
  ADD CONSTRAINT `fk_Zamowienia_Konta` FOREIGN KEY (`Konta_id_konta`) REFERENCES `Konta` (`id_konta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Zamowienia_Restauracja` FOREIGN KEY (`Restauracja_id_restauracja`) REFERENCES `Restauracja` (`id_restauracja`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Zamowienia_Status1` FOREIGN KEY (`Status_id_status`) REFERENCES `Status` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
