# Aplikacja do Składania Zamówień
# Aplikacja Webowa

 Aplikacja Webowa, to zaplecze informatyczne oraz główna część sytemu Aplikacji do Składania Zamówień.

 Aplikacja wykorzystuje platformę programistyczną [Simple MVC Framework](http://simplemvcframework.com/).

## Wymagania

 Aplikacja do poprawnego działania wymaga:

 - Serwera www - Apache Web Serwer - z obsługą modułu mod rewrite.
 - Interpretera PHP w wersji co najmniej 5.3
 - Serwera bazodanowego MySQL
 
## Instalacja

 1. Po pobraniu kodu źródłowego aplikacji, należy umieścić go w folderze głównym strony (np. `htdocs`)
 2. Przeprowadzić konfigurację witryny poprzez edycję pliku `app/core/config.php` (przykładowa konfiguracja znajduje się w pliku `app/core/config.php.sample`, który należy skopiować)
 3. W przypadku umieszczenia strony poza główną domeną należy ustawić ścieżkę do aplikacji w pliku `.htaccess` edytując parametr `RewriteBase` (np. gdy strona będzie hostowana pod adresem http://localhost/asz/, należy ustawić `RewriteBase /asz/`)
 4. Utworzyć bazę danych dla aplikacji, a następnie wybierając utworzoną wcześniej bazę wywołać skrypt `asz.sql` jako zapytanie SQL

## API

 Do systemu możliwy jest dostęp z poziomu API. Dokumentacja API jest dostępna tutaj -  [https://bitbucket.org/aplikacja-skadanie-zamowien/aplikacja-webowa/wiki/API](API).