<?php 

class Message{ 
	
	public function show_info($message, $class='info'){
	
		echo '<div class="'.$class.'">'.$message.'</div>';
	}
	
	public function show_success($message){

		echo '<div class="success">'.$message.'</div>';
	}	

	public function show_warning($message){

		echo '<div class="warning">'.$message.'</div>';
	}
	
	public function show_error($message){

		echo '<div class="error">'.$message.'</div>';
	}
	
	private function show_validation_message($error){
		
		
	}
	
	public function show_validation_error($errors, $field, $class='error'){
		
		if(!is_array($errors)) return;
		
		foreach($errors as $error){
			
			if(strcmp($field, $error['field']) != 0 ) continue;
			
			switch($error['rule']){

				case('validate_required'):
					$message = "Pole jest wymagane";
					break;
				case('validate_valid_email'):
					$message = "Wymagany jest poprawny adres email";
					break;
				case('validate_password_confirm'):
					$message = "Wprowadzone hasła różnią się";
					break;
				default:
					$message = $error['rule'];
			}
			
			echo '<label for="'.$error['field'].'" class="'.$class.'">'.$message.'</label>';
		}
	}
	
}