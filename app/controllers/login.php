<?php

class Login extends Controller{
	
	private $login;

	public function __construct(){
		parent::__construct();
		
		$this->login = $this->loadModel('login_model');
	}
	
	public function login(){
		
		if($this->login->is_loged_in()){url::redirect('user');}
		
		if(isset($_POST['submit'])){
			
			$validated = $this->login->validate($_POST);
			
			if($validated === TRUE){

				$data = $this->login->get_data($_POST);
				if($this->login->login($data['login'], $data['password'])){
					
					url::redirect('user');
	
				} else {
	
					$data['error'] = true;
				}
			}
		}

		$data['title'] = 'Login user';
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('login/login',$data, $validated);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function logout(){

		$this->login->logout();
		Url::redirect('user');
	}	
	
	public function password(){
		
		if(isset($_POST['submit'])){
			
			$data['password'] = $_POST['password'];
			$data['code'] = $this->login->password($data['password']);
		}

		$data['title'] = 'Generowanie hasła';

		$this->view->rendertemplate('header', $data);
			$this->view->render('login/password',$data,$err);
		$this->view->rendertemplate('footer',$data);
	}

}