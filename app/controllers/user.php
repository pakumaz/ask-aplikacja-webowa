<?php

class User extends Controller{
	
	private $user;
	private $role;

	public function __construct(){
		parent::__construct();
		
		$this->user = $this->loadModel('user_model');
		$this->role = $this->loadModel('role_model');
		$this->login = $this->loadModel('login_model');
		
		if($this->login->is_loged_in() == false){url::redirect('login');}
	}
	
	// User home page
	public function user(){

		$data['title'] = 'User page';
		
		$data['users'] = $this->user->get_all(); 
		
		$this->view->rendertemplate('header',$data);
		$this->view->render('user/user',$data);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function add(){
		
		if(isset($_POST['submit'])){

			$data = $this->user->get_post_data();
			$validated = $this->user->validate($_POST);
			
			if($validated === true){				

				$this->user->add($data['login'], $data['password'], $data['email'], $data['name'], $data['surname'], $data['phone'], $data['type']);
				$data['success'] = true;
			
			}
			
		} else {
			
			$validated = true;
		}
		
		$data['title'] = 'Dodanie użytkownika';
		$data['action'] = 'dodano';
		$data['button'] = 'Dodaj';
		$data['roles'] = $this->role->get_all();
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('user/form',$data,$validated);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function edit($id){	
		
		if(isset($_POST['submit'])){

			$data = $this->user->get_post_data();
			$validated = $this->user->validate($_POST, false);
			
			if($validated === true){			

				$this->user->edit($id, $data['login'], $data['password'], $data['email'], $data['name'], $data['surname'], $data['phone'], $data['type']);
				$data['success'] = true;
			}			
		
		} else{
			
			$data = $this->user->get($id);
			$validated = true;
		}
		
		$data['title'] = 'Edycja użytkownika';
		$data['action'] = 'zaktualizowano';
		$data['button'] = 'Edytuj';
		$data['roles'] = $this->role->get_all();
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('user/form',$data,$validated);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function del($id){
		
		if(strcmp($_GET['action'], 'delete') == 0){
			
			$this->user->delete($id);
			
			url::redirect('user');
		
		}elseif(strcmp($_GET['action'], 'cancel') == 0){
			
			url::redirect('user');
			
		} else {

			$data['title'] = 'Kasowanie użytkownika';
			
			$data['id'] = $id;
			
			$this->view->rendertemplate('header', $data);
			$this->view->render('user/del',$data,$err);
			$this->view->rendertemplate('footer',$data);
		}
	}

}