<?php

class Order extends Controller {
	
	private $order;
	private $login;

	public function __construct(){
		
		parent::__construct();

		$this->order = $this->loadModel('order_model');
		$this->login = $this->loadModel('login_model');
		
		if($this->login->is_loged_in() == false){url::redirect('login');}
		
	}
	
	public function index(){

		$data['title'] = 'Lista aktualnych zamówień';
		
		$data['orders'] = $this->order->get_unrealized();
		
		$this->view->rendertemplate('header',$data);
		$this->view->render('order/orders',$data,$error);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function place(){

		if(isset($_POST['submit'])){
		
			$data = $this->order->get_data($_POST);
			$validated = $this->order->validate($data);
				
			if($validated === true){
				
				$user = $this->login->get_id();
				$this->order->place($user, $data);
				$data['success'] = true;					
			}
				
		} else {
				
			$validated = true;
			$data['dish'] = array();
		}
		
		$data['title'] = 'Składanie zamówienia';
		$data['action'] = 'złożono';
		$data['button'] = 'Złóż';
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('order/form',$data,$validated,true);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function details($id){
		
		echo "details $id";
	}
	
	public function update($id){
		
		if(isset($_POST['submit'])){
		
			$data = $this->order->get_data($_POST);
			$validated = $this->order->validate($data);
		
			if($validated === true){
		
				$this->order->update($id, $data);
				$data['success'] = true;
			}
		
		} else {
		
			$validated = true;
			$data = $this->order->get($id);
		}
		
		$data['title'] = 'Składanie zamówienia';
		$data['action'] = 'złożono';
		$data['button'] = 'Złóż';
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('order/form',$data,$validated,true);
		$this->view->rendertemplate('footer',$data);
	}
	
	public function change_status($id,$status){

		$data['title'] = 'Zmiana statusu zamówienia';
		
		$updated = $this->order->change_status($id,$status);
		
		$this->view->rendertemplate('header', $data);
		
		if($updated)
			Message::show_info("Zamówienie zostało oznaczone jako $status");
		else
			Message::show_error("Wybrany status zamówienia nie istnieje!");
		
		$this->view->rendertemplate('footer',$data);
	}
}
