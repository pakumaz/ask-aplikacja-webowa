<?php

class Login extends Controller{
	
	private $login;
	
	public function __construct(){
		parent::__construct();
	
		$this->login = $this->loadModel('login_model');
	}
	
	function index(){
		
		echo 'login_required';
	}
	
	function login(){
		
		$data = $this->login->get_data($_POST);
		
		if($this->login->login($data['login'], $data['password'])){
			
			echo "login_accepted";
			
		} else {
			
			echo "login_rejected";
		}
	}
	
	function logout(){
		
		$this->login->logout();
		echo "sesion_destroyed";	
	}
}