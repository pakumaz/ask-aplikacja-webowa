<?php

class Order extends Controller {

	private $order;
	private $login;
	
	public function __construct(){
	
		parent::__construct();
	
		$this->order = $this->loadModel('order_model');
		$this->login = $this->loadModel('login_model');
	
		if($this->login->is_loged_in() == false){url::redirect('login');}
	
	}
	
	public function index(){
		
		$orders = $this->order->get_unrealized();
		echo json_encode($orders);
	}
	
	public function status_list(){
		
		$order = $this->order->get_avaiable_statuses();
		echo json_encode($order);		
	}
	
	public function change_status($id, $status){		

		$updated = $this->order->change_status($id,$status);
		
		if($updated)
			echo 'status_updated';
		else
			echo 'status_rejected';
	}
	
	public function place(){
		
		$data = $this->order->get_data($_POST);
		$validated = $this->order->validate($data);
			
		if($validated === true){
			
			$user = $this->login->get_id();
			$this->order->place($user, $data);
			echo "order_accepted";
								
		} else {

			echo "order_rejected";
		}
	}
	
}