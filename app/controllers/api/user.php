<?php

class User extends Controller {

	private $user;
	private $role;
	
	public function __construct(){
		parent::__construct();
	
		$this->user = $this->loadModel('user_model');
		$this->role = $this->loadModel('role_model');
		$this->login = $this->loadModel('login_model');
	
		if($this->login->is_loged_in() == false){url::redirect('api');}
	}
	
	function view($id){
		
		$data = $this->user->get($id);
		echo json_encode($data);		
	}
	
	function view_all(){

		$data = $this->user->get_all();
		echo json_encode($data);		
	}
	
}