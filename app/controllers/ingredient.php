<?php

class Ingredient extends Controller
{
		private $ingredient;

	public function __construct(){
		parent::__construct();
		
		$this->ingredient = $this->loadModel('ingredient_model');
		$this->login = $this->loadModel('login_model');
		
		if($this->login->is_loged_in() == false){url::redirect('login');}
	}
	
	// wyświetlenie wszystkich składników
	public function ingredient()
	{		
		$data['title'] = 'Wszystkie składniki';
		
		$data['ingredients'] = $this->ingredient->get_all(); 
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('ingredient/ingredient',$data);
		$this->view->rendertemplate('footer',$data);
	}
	
	// dodanie nowego składnika
	public function add()
	{
		if(isset($_POST['submit']))
		{
			$data = $this->ingredient->get_post_data();
			$this->ingredient->add($data['name'], $data['number']);
			$data['success'] = true;
		}
		
		$data['title'] = 'Dodanie składnika';
		$data['action'] = 'dodano';
		$data['button'] = 'Dodaj';
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('ingredient/form',$data);
		$this->view->rendertemplate('footer',$data);
	}
	
	// edycja składnika
	public function edit($id)
	{			
		if(isset($_POST['submit']))
		{
			$data = $this->ingredient->get_post_data();
			$this->ingredient->edit($id, $data['name'], $data['number']);
			$data['success'] = true;
		} 
		
		$data['title'] = 'Edycja składników';
		$data['action'] = 'zaktualizowano';
		$data['button'] = 'Edytuj';
		
		$this->view->rendertemplate('header', $data);
		$this->view->render('ingredient/form',$data);
		$this->view->rendertemplate('footer',$data);
	}
	
	// usunięcie składnika
	public function delete($id)
	{
		if(strcmp($_GET['action'], 'delete') == 0)
		{	
			$this->ingredient->delete($id);
			url::redirect('ingredient');
		
		}
		elseif(strcmp($_GET['action'], 'cancel') == 0)
		{
			url::redirect('user');
		}
		else 
		{
			$data['title'] = 'Usuwanie składnika';
			$data['id'] = $id;
			
			$this->view->rendertemplate('header', $data);
			$this->view->render('ingredient/delete',$data,$err);
			$this->view->rendertemplate('footer',$data);
		}
	}

}