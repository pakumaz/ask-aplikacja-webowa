<?php

class Order_model extends Model{
	
	public function validate(array $data){
		
		$validator = new GUMP();
		
		$rules = array(
			
			'quantity' => 'required|integer',
			'table' => 'required'
		);
		
		return $validator->validate($data,$rules);
	}
	
	public function get_data(array $data){
		
		$output = array();
		
		$output['quantity'] = $data['quantity'];
		$output['table'] = $data['table'];
		
		if(is_array($data['dish']))
			$output['dish'] = array_filter($data['dish']);
		else
			$output['dish'] = array();

		$output['comment'] = $data['comment'];
		$output['note'] = $data['note'];		

		return $output;		
	}

	public function get($id){
		
		$orders =  $this->_db->select('SELECT
				
				ilosc quantity, 
				Konta_id_konta place_by, 
				nr_stolika "table", 
				data_zlozenia placed, 
				data_wydania served,
				uwagi_klienta comment,
				notatki note,
				Status_id_status status				
				
				FROM Zamowienia WHERE id_zamowienia = :id', array('id' => $id), PDO::FETCH_ASSOC);
		
		$order = $orders[0];
		
		$order['dish'] = $this->_db->select('SELECT
				
				Dania_id_dania id
				
				FROM `Dania-Zamowienia` WHERE Zamowienia_id_zamowienia = :id', array('id' => $id), PDO::FETCH_COLUMN);
		
		return $order;
	}
	
	public function get_all(){		

		return $this->_db->select('SELECT 
				
				id_zamowienia id,				
				ilosc quantity, 
				Konta_id_konta place_by, 
				nr_stolika "table", 
				data_zlozenia placed, 
				data_wydania served, 
				Status_id_status status 
				
				FROM Zamowienia');
	}
	
	public function get_unrealized(){
		
		return $this->get_all();
	}

	public function place($user, $data){
		
		$order_data = array(
			
			'ilosc' => $data['quantity'],
			'nr_stolika' => $data['table'],
			'Konta_id_konta' => $user,
			'uwagi_klienta' => $data['comment'],
			'notatki' => $data['note']
		);
		
		$order = $this->_db->insert('Zamowienia', $order_data);
		
		$order_id = $this->_db->lastInsertId();
		
		foreach($data['dish'] as $dish){
			
			$dish_data = array(

				'Zamowienia_id_zamowienia' => $order_id,
				'Dania_id_dania' => $dish
			);
			
			$this->_db->insert('`Dania-Zamowienia`', $dish_data);
		}
	}
	
	public function get_avaiable_statuses(){
		
		return $this->_db->select('SELECT status FROM Status', array(), PDO::FETCH_COLUMN);		
	}
	
	public function update($id, $data){

		$order_data = array(
					
			'ilosc' => $data['quantity'],
			'nr_stolika' => $data['table'],
			'Konta_id_konta' => 0,
			'uwagi_klienta' => $data['comment'],
			'notatki' => $data['note']
		);
		
		$order = $this->_db->update('Zamowienia', $order_data, array('id_zamowienia' => $id));
		
		$this->_db->delete('`Dania-Zamowienia`', array('Zamowienia_id_zamowienia' => $id, 0) );
		
		foreach($data['dish'] as $dish){
				
			$dish_data = array(
		
					'Zamowienia_id_zamowienia' => $id,
					'Dania_id_dania' => $dish
			);
				
			$this->_db->insert('`Dania-Zamowieniaienia`', $dish_data);
		}
	}
	
	public function change_status($id,$status){
		
		$select = $this->_db->select('SELECT id_status FROM Status WHERE status = :status', array('status' => $status), PDO::FETCH_COLUMN);
		
		if( count($select) > 0 ){	
		
			$id_status = $select[0];
		
			$this->_db->update('Zamowienia', array('Status_id_status' => $id_status), array('id_zamowienia' => $id));
			
			return true;
		
		} else { 
			
			return false;
		
		}
	}
}
