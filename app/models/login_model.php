<?php

class Login_model extends Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function get_data($input){
		
		return array(
	
			'login' => $input['login'],
			'password' => $input['password'],
		);
	}
	
	public function validate($input){
		
		$validator = new GUMP();
		
		$rules = array(
			
			'login' => 'required',
			'password' => 'required'
		);
		
		return $validator->validate($input, $rules);
	}
	
	public function login($login, $password){		

		if(Password::verify($password, $this->get_hash($login))){

			Session::set('login', true);
			Session::set('user', $login);
			return true;
		
		} else {
		
			return false;
		}
	}
	
	public function logout(){

		return Session::destroy();
	}
	
	public function is_loged_in(){
		
		return Session::get('login');
	}
	
	public function get_username(){

		return Session::get('user');
	}
	
	public function get_id(){
		
		$login = $this->get_username();
	
		$data = $this->_db->select("SELECT id_konta id FROM ".PREFIX."Konta WHERE login = :login", array(':login' => $login), PDO::FETCH_COLUMN);
		
		return $data[0];
	}
	
	public function password($password){
		
		return Password::make($password);
	}
	
	private function get_hash($login){
		
		$data = $this->_db->select("SELECT haslo FROM ".PREFIX."Konta WHERE login = :login", array(':login' => $login));		
		return $data[0]->haslo;
	}

}