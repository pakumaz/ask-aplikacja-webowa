<?php

class Ingredient_model extends Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_all()
	{
		return $this->_db->select("SELECT id_skladniki id, nazwa name, ilosc number FROM ".PREFIX."Skladniki");		
	}
	
	public function get_post_data()
	{
		$data = array();
		$data['name'] = $_POST['name'];
		$data['number'] = $_POST['number'];
		
		return $data;
	}
	
	public function add($name, $number)
	{
		$data = array(
				'nazwa' => $name,
				'ilosc' => $number,
		);
		
		$this->_db->insert(PREFIX.'Skladniki', $data);
	}
	
	public function edit($id, $name, $number){
		
		$data = array(
			'nazwa' => $name,
			'ilosc' => $number,
		);
		
		$this->_db->update(PREFIX.'Skladniki', $data, array('id_skladniki' =>$id), array('nazwa' => $name), array('ilosc' => $number));		
	}
	
		public function delete($id){
		
		return $this->_db->delete(PREFIX."Skladniki", array( 'id_skladniki' => $id));
	}

}