<?php

class User_model extends Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function get($id){
		
		$data = $this->_db->select("SELECT id_konta id,	login, imie name, nazwisko surname,	email, telefon phone, Typy_uzytkownikow_id_typ type 
				FROM ".PREFIX."Konta WHERE id_konta = :id", array(':id' => $id), PDO::FETCH_ASSOC);
		
		return $data[0];
	}
	
	public function get_post_data(){
		
		$data = array();

		$data['login'] = $_POST['login'];
		$data['password'] = $_POST['password'];
		$data['password_rep'] = $_POST['password_rep'];
		$data['email'] = $_POST['email'];
		$data['name'] = $_POST['name'];
		$data['surname'] = $_POST['surname'];
		$data['phone'] = $_POST['phone'];
		$data['type'] = $_POST['type'];
		
		return $data;
	}
	
	public function validate(array $input, $require_password = true){
		
		$validator = new GUMP();
		
		$rules = array(
				
			'login'	=> 'required',
			'email'	=> 'required|valid_email',
			'type' 	=> 'required'
		);
		
		if($require_password || !empty($input['password']) || !empty($input['password_rep']) ){
	
			$validator->add_validator("password_confirm", function($field, $input, $param) {
				return strcmp($input[$field],$input[$param]) == 0;
			});
			
			$rules['password'] = 'required|password_confirm,password_rep';
		}		
			
		return $validator->validate($input, $rules);
	}
	
	public function get_all(){
		
		return $this->_db->select("SELECT id_konta id, login, imie name, nazwisko surname, email, telefon phone FROM ".PREFIX."Konta");		
	}
	
	public function add($login, $password, $email, $name, $surname, $phone, $type){

		$data = array(
					
				'login' => $login,
				'haslo' => Password::make($password),
				'email' => $email,
				'imie' => $name,
				'nazwisko' => $surname,
				'telefon' => $phone,
				'Typy_uzytkownikow_id_typ' => $type,
		);
		
		$this->_db->insert(PREFIX.'Konta', $data);
	}
	
	public function edit($id, $login, $password, $email, $name, $surname, $phone, $type){
		
		$data = array(
				
				'email' => $email,
				'imie' => $name,
				'nazwisko' => $surname,
				'telefon' => $phone,
				'Typy_uzytkownikow_id_typ' => $type,
		);
		
		if(!empty($password)){
			
			$data['haslo'] = Password::make($password);
		}		
		
		$this->_db->update(PREFIX.'Konta', $data, array('id_konta' => $id));		
	}
	
	public function delete($id){
		
		return $this->_db->delete(PREFIX."Konta", array( 'id_konta' => $id));
	}

}