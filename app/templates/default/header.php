<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $data['title'].' - '.SITETITLE; //SITETITLE defined in index.php?></title>
	<link href="<?php echo url::get_template_path();?>css/style.css" rel="stylesheet">
	<link href="<?php echo url::get_template_path();?>css/menu.css" rel="stylesheet">
</head>
<body>

<div id='wrapper'>
	
	<h2><a href="/"><?php echo SITETITLE; ?></a></h2>
	
	<div id="menu"><nav><ul>
	
		<li><a href="/user">Użytkownicy</a><ul>
		
			<li><a href="/user">Zarządzaj użytkownikami</a></li>
			<li><a href="/user/add">Dodaj użytkownika</a></li>
			<li><a href="/user/password">Generuj hasło</a></li>
		
		</ul></li>
		
		<li><a href="/ingredient">Składniki</a><ul>
		
			<li><a href="/ingredient">Wszystkie składniki</a></li>		
			<li><a href="/ingredient/add">Dodaj nowy składnik</a></li>
		
		</ul></li>
		
		<li><a href="/order">Zamówienia</a><ul>
		
			<li><a href="/order">Zarządzaj zamówieniami</a></li>		
			<li><a href="/order/place">Złóż zamówienie</a></li>
		
		</ul></li>
				
		<li><a href="/sample">Strona przykładowa</a></li>

		<li class="logout"><a href="/logout">Wyloguj</a></li>
		
	
	</ul></nav></div>
	
	<div id="content" class="clear">
	
		<h3><?php echo $data['title']; ?></h3>
