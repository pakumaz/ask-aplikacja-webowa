<?php

set_exception_handler('logger::exception_handler');
set_error_handler('logger::error_handler');

//set timezone
date_default_timezone_set('Europe/London');

//site address
define('DOMAIN','http://localhost');
define('DIR','/');

//database details ONLY NEEDED IF USING A DATABASE
define('DB_TYPE','mysql');
define('DB_HOST','156.17.43.89');
define('DB_NAME','asz');
define('DB_USER','asz');
define('DB_PASS','EwHSmVpbRKVzhqWB');
define('PREFIX','');

//set prefix for sessions
define('SESSION_PREFIX','smvc_');

//optionall create a constant for the name of the site
define('SITETITLE','Simple MVC Framework v2');

//set the default template
Session::set('template','default');
