<?php

class View {

	public function render($path,$data = false, $error = false, $debug = false){
		
		if($debug == true){
			
			echo "<pre class=\"debug\">";
			echo "data: "; print_r($data);
			echo "error: "; print_r($error);
			echo "</pre>";
		}
		
		require "app/views/$path.php";
	}

	public function rendertemplate($path,$data = false){
		require "app/templates/".Session::get('template')."/$path.php";
	}

}