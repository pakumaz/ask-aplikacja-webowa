<?php
//turn on output buffering
ob_start();

function autoloader($class) {

   $filename = "app/controllers/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

   $filename = "app/core/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

   $filename = "app/helpers/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

}

//run autoloader
spl_autoload_register('autoloader');
//start sessions
Session::init();

if(file_exists('app/core/config.php')) include ('app/core/config.php');
else include ('app/core/config.openshift.php');
