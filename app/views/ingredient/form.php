	
	<?php if(isset($data['success'])) Message::show_success('Pomyślnie '.$data['action'].' produkt:  '.$data['name'].', ilość: '.$data['number']); ?>	
	<?php if($error == true) Message::show_error("Formularz został wypełniony błędnie"); ?>
	
	
	<form action="" method="post">

		<div>
			<label>Nazwa produktu:</label>			
			<?php Message::show_validation_error($error, 'name'); ?>
			<input type="text" id="name" name="name">
		</div>
		
		<div>
			<label>Ilość:</label>
			<?php Message::show_validation_error($error, 'number'); ?>
			<input type="text" id="number" name="number">
		</div>
			
		<div>
			<input type="submit" name="submit" value="Dodaj produkt">
		</div>
		
	</form>
	