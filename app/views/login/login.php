	
	<?php if(isset($data['error'])) Message::show_error('Podano błędny login i/lub hasło'); ?>
	
	<form action="" method="post">

		<div>
			<label>Login</label>			
			<?php Message::show_validation_error($error, 'login'); ?>
			<input type='text' id='login' name='login'>
		</div>
		
		<div>
			<label>Hasło</label>
			<?php Message::show_validation_error($error, 'password'); ?>
			<input type='password' id='password' name='password'>
		</div>
			
		<div>
			<input type='submit' name='submit' value='Zaloguj'>
		</div>
		
	</form>
	