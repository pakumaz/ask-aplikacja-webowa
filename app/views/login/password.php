
	<form action="" method="post">
	
		<div>			
			<label>Hasło</label>
			<?php Message::show_validation_error($error, 'password'); ?>
			<input type="text" id="password" name="password" value="<?php echo $data['password']; ?>">
		</div>
		
		<div>			
			<label>Szyfr</label>
			<?php Message::show_validation_error($error, 'code'); ?>
			<input type="text" id="code" name="code" value="<?php echo $data['code']; ?>" readonly>
		</div>
		
		<div>
			<input type="submit" name="submit" value="Generuj hasło">
		</div>
	
	</form>