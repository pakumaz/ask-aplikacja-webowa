	
	<?php if(isset($data['success'])) Message::show_success('Pomyślnie '.$data['action'].' użytkownika '.$data['name'].' '.$data['surname']); ?>	
	<?php if($error !== true) Message::show_error("Formularz został wypełniony błędnie"); ?>
	
	<form action="" method="post">

		<div>
			<label>Login</label>
			<?php Message::show_validation_error($error, 'login'); ?>
			<input type="text" id="login" name="login" value="<?php echo $data['login']; ?>">
		</div>
		
		<div>
			<label>Hasło</label>
			<?php Message::show_validation_error($error, 'password'); ?>
			<input type="password" id="password" name="password">		
		</div>
		
		<div>
			<label>Powtórz Hasło</label>
			<input type="password" id="password_rep" name="password_rep">		
		</div>				
		
		<div>
			<label>Imię</label>
			<?php Message::show_validation_error($error, 'name'); ?>
			<input type="text" id="name" name="name" value="<?php echo $data['name']; ?>">		
		</div>
		
		<div>
			<label>Nazwisko</label>
			<?php Message::show_validation_error($error, 'surname'); ?>
			<input type="text" id="surname" name="surname" value="<?php echo $data['surname']; ?>">		
		</div>
		
		<div>
			<label>Telefon</label>
			<?php Message::show_validation_error($error, 'phone'); ?>
			<input type="phone" id="phone" name="phone" value="<?php echo $data['phone']; ?>">		
		</div>
		
		<div>
			<label>E-mail</label>
			<?php Message::show_validation_error($error, 'email'); ?>
			<input type="email" id="email" name="email" value="<?php echo $data['email']; ?>">		
		</div>
		
		<div>
			<label>Typ</label>
			<?php Message::show_validation_error($error, 'type'); ?>			
			<select name="type" >
				<?php foreach($data['roles'] as $role)
					if($data['type'] == $role->id)
						echo "<option value=\"$role->id\" selected>$role->name</option>";
					else
						echo "<option value=\"$role->id\">$role->name</option>";
				?>
			</select>	
		</div>
		
		<div>
			<input type="submit" name="submit" value="<?php echo $data['button']; ?>">		
		</div>
		
	</form>
	