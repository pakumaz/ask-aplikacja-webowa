	
	<?php if(isset($data['success'])) Message::show_success("Pomyslnie ".$data['action']." zamówienie"); ?>	
	<?php if($error !== true) Message::show_error("Formularz został wypełniony błędnie"); ?>
	
	<form action="" method="post">

		<div>
			<label>Stolik</label>
			<?php Message::show_validation_error($error, 'table'); ?>
			<input type="text" id="table" name="table" value="<?php echo $data['table']; ?>">
		</div>
		
		<div>
			<label>Ilość</label>
			<?php Message::show_validation_error($error, 'quantity'); ?>
			<input type="text" id="quantity" name="quantity" value="<?php echo $data['quantity']; ?>">
		</div>
		
		<div>
			<label>Potrawa</label>
			<?php Message::show_validation_error($error, 'dish'); ?>
			<?php foreach ($data['dish'] as $dish) : ?>
			<input type="text" id="dish" name="dish[]" value="<?php echo $dish; ?>">
			<?php endforeach; ?>
			
			<input type="text" id="dish" name="dish[]">
			<input type="text" id="dish" name="dish[]">
			<input type="text" id="dish" name="dish[]">						
		</div>		
		
		<div>
			<label>Uwagi klienta</label>
			<?php Message::show_validation_error($error, 'comment'); ?>
			<textarea type="textarea" id="comment" name="comment" ><?php echo $data['comment']; ?></textarea>		
		</div>
		
		<div>
			<label>Notatki</label>
			<?php Message::show_validation_error($error, 'note'); ?>
			<textarea type="textarea" id="note" name="note" ><?php echo $data['note']; ?></textarea>		
		</div>
		
		<div>
			<input type="submit" name="submit" value="<?php echo $data['button']; ?>">		
		</div>
		
	</form>
	